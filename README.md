---
title: Whisper WebGPU
emoji: 🎤
colorFrom: blue
colorTo: indigo
sdk: static
pinned: true
models:
  - onnx-community/whisper-tiny
  - onnx-community/whisper-tiny.en
  - onnx-community/whisper-small
  - onnx-community/whisper-small.en
  - onnx-community/whisper-base
  - onnx-community/whisper-base.en
  - onnx-community/distil-small.en
thumbnail: >-
  https://huggingface.co/spaces/Xenova/whisper-webgpu/resolve/main/banner.png
---

Check out the configuration reference at https://huggingface.co/docs/hub/spaces-config-reference


### Instruction

1. Move to root project
2. Execute `docker-compose up --build -d`
3. Open http://localhost:8080